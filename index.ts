import express from "express";
import cors from "cors";
import {Datastore} from "@google-cloud/datastore";


const app = express();
app.use(express.json());
app.use(cors());
const datastore = new Datastore();

/**
 * check for valid email. if valid => check password
 * if password is valid => respond with fname and lname
 */
app.patch("/users/login", async (req, res) =>
{
    const query = datastore.createQuery("Employee");
    const response = await datastore.runQuery(query);

    const email = req.body["email"];
    const password = req.body["password"]

    for (let i = 0; i < response[0].length; i++)
    {
        if (email === response[0][i]["email"])
        {
            if (password === response[0][i]["password"])
            {
                res.status(200).send({fname:response[0][i]["fname"], lname:response[0][i]["lname"]});
                return;
            }
        }
    }
    res.status(404).send(false);
});

app.get("/users/:email/verify", async (req, res) =>
{
    const key = datastore.key(['Employee', req.params.email]);
    const employee = await datastore.get(key);
    if(employee[0])
    {
        res.status(200).send(true);
    }
    else
    {
        res.status(404).send(false);
    }
});

app.get("/users", async (req,res) =>
{
    const query = datastore.createQuery("Employee");
    const response = await datastore.runQuery(query);

    const emails:any = response[0].map((e) => {return e.email});
    
    res.status(200).send(emails);
})

const PORT = process.env.PORT || 3002;

app.listen(PORT, () => (console.log("Application started")));
